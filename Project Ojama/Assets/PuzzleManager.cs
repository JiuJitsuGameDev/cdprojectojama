﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour {

    public string[] keys;
    List<string> keyList = new List<string>();
    GameObject button;
    GameObject door;    
    GameObject[] drones;
    GameObject[] panels;

	void Start () {
        button = GameObject.FindGameObjectWithTag("Button");
        //Drone Management
        drones = GameObject.FindGameObjectsWithTag("Drone");
        int i = 0;
        foreach(GameObject d in drones)
        {
            DroneAudioSelector das = d.GetComponent<DroneAudioSelector>();
            das.tone = keys[i];
            keyList.Add(keys[i]);
            das.UpdateAudio(das.tone);
            print(i + " Corresponding Tone = " + d.GetComponent<DroneAudioSelector>().tone);
            
            i++;            
        }

        //Panel Management
        panels = GameObject.FindGameObjectsWithTag("Panel");

        door = GameObject.FindGameObjectWithTag("Door");
        
    }
    IEnumerator Open()
    {
        float timer = 100f;
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            door.GetComponent<OpenDoor>().Open();
        }

        yield return new WaitForSeconds(0);
    }
	
	// Update is called once per frame
	void Update () {
		if(keyList.Contains(panels[0].GetComponent<PanelControl>().note) && keyList.Contains(panels[1].GetComponent<PanelControl>().note) && keyList.Contains(panels[2].GetComponent<PanelControl>().note))
        {
            print("OPENED");
            if (button.GetComponent<Button>().isPressed) {
                StartCoroutine(Open());
                
            }
        }
	}
}
