﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour {

    List<SteeringBehaviour> behaviours = new List<SteeringBehaviour>();

    [HideInInspector]
    public Vector3 force = Vector3.zero;
    [HideInInspector]
    public Vector3 velocity = Vector3.zero;
    [HideInInspector]
    public Vector3 acceleration = Vector3.zero;

    public float mass = 1f;
    public float maxSpeed = 1f;

	void Start () {
        SteeringBehaviour[] behaviours = GetComponents<SteeringBehaviour>();
        foreach(SteeringBehaviour b in behaviours)
        {
            this.behaviours.Add(b);
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        force = Calculate();
        Move();
	}

    Vector3 Calculate()
    {
        force = Vector3.zero;
        foreach(SteeringBehaviour b in behaviours)
        {
            if (b.isActiveAndEnabled)
            {
                force += b.Calculate() * b.weight;
            }
        }
        return force;
    }

    void Move()
    {
        Vector3 tempAcceleration = force / mass;
        float smoothRate = Mathf.Clamp(9.0f * Time.fixedDeltaTime, 0.15f, 0.4f) / 2.0f;
        acceleration = Vector3.Lerp(acceleration, tempAcceleration, smoothRate);

        velocity += acceleration * Time.fixedDeltaTime;
        velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

        Vector3 globalUp = new Vector3(0, 0.2f, 0);
        Vector3 accelUp = acceleration * 0.05f;
        Vector3 bankUp = accelUp + globalUp;
        smoothRate = Time.fixedDeltaTime * 3.0f;
        Vector3 tempUp = transform.up;
        tempUp = Vector3.Lerp(tempUp, bankUp, smoothRate);

        if (velocity.magnitude > 0.0001f)
        {
            transform.LookAt(transform.position + velocity, tempUp);
            velocity *= 0.99f;
        }
        transform.position += velocity * Time.fixedDeltaTime;
    }

}
