﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seek : SteeringBehaviour {

    [HideInInspector]
    public Vector3 target = Vector3.zero;

    public override Vector3 Calculate()
    {
        Vector3 desired = target - transform.position;
        desired.Normalize();
        desired *= boid.maxSpeed;
        return desired - boid.velocity;
    }

}
