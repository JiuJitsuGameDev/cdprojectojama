﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Requirements:
 *Box Collider
 *User Input
 *RigidBody
*/
[RequireComponent(typeof(CharacterController))]

public class Movement : MonoBehaviour {

    CharacterController controller;
    Transform myTransform;

    public float    walkSpeed       =   60.0f,
                    runningSpeed    =   100.0f,
                    jumpSpeed       =   4.0f,
                    gravity         =   10.0f;

    private float   speed;

    public bool     airControl      =   true;
    public bool     enableRunning   =   false;
    private bool    grounded        =   false;
    private bool    limtDiagonalSpd =   false;
    private bool    playerControl   =   false;
    private int     jumpTimer;
    public int      antiHopFactor   =   1;
    public float    antiBumpFactor  = .75f;

    public string   walkAxisName,
                    straphAxisName,
                    jumpButtonName,
                    runButton;

    private Vector3 moveDirection = Vector3.zero;

	void Start () {
        speed = walkSpeed;
        myTransform = transform;
        controller = GetComponent<CharacterController>();
        jumpTimer = antiHopFactor;
    }

    void FixedUpdate()
    {       
        float inputX = Input.GetAxis(straphAxisName);
        float inputY = Input.GetAxis(walkAxisName);

        float inputModFactor = (inputX != 0.0f &&
                                inputY != 0.0f &&
                                limtDiagonalSpd) ? .7071f : 1.0f;
        if (grounded)
        {
            speed = Input.GetButton(runButton) ?  runningSpeed : walkSpeed;
            moveDirection = new Vector3(inputX * inputModFactor,
                                        0,
                                        inputY * inputModFactor)
                                        * speed;

            moveDirection = transform.TransformDirection(moveDirection);
            Jump();
            Gravity();
        }
        moveDirection.y -= gravity * Time.fixedDeltaTime;
        grounded = (controller.Move(moveDirection * Time.fixedDeltaTime) & CollisionFlags.Below) != 0;
    }

    void Gravity()
    {
        controller.Move(moveDirection * Time.fixedDeltaTime);
    }

    void Jump()
    {
        if (!Input.GetButton("Jump"))
            jumpTimer++;
        else if (jumpTimer >= antiHopFactor)
        {
            moveDirection.y = jumpSpeed;
            jumpTimer = 0;
        }
    }
}
