﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class DroneAudioSelector : MonoBehaviour {

    public string tone;
    AudioSource audioSource;
    
	// Use this for initialization
	void Awake () {
        audioSource = this.GetComponent<AudioSource>();
        
	}
        
    public void UpdateAudio(string s)
    {
        audioSource.Stop();
        string path = "Audio\\"+ s;     
        AudioClip audioClip = Resources.Load(path, typeof(AudioClip)) as AudioClip;
        audioSource.clip = audioClip;
        audioSource.Play();
    }

}
