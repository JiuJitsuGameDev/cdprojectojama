﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour {

    Quaternion originalRotation, originalCamRotation;

    private List<float> rotArrayX = new List<float>();
    private List<float> rotArrayY = new List<float>();

    public float    minX = -360F,
                    maxX = 360F, 
                    minY = -85F,
                    maxY = 85F,
                    rotX = 0F,
                    rotY = 0F,
                    avrgXRot = 0F,
                    avrgYRot = 0F,
                    framesOfSmoothing = 5,
                    sensitivityX = 10F,
                    sensitivityY = 9F;

    void Start () {
        originalRotation = transform.localRotation;
        originalCamRotation = Camera.main.transform.localRotation;
    }
	
	void Update () {

        avrgXRot = 0f;
        avrgYRot = 0f;

        rotX += Input.GetAxis("Mouse X") * sensitivityX * Time.timeScale;
        rotY += Input.GetAxis("Mouse Y") * sensitivityY * Time.timeScale;

        rotArrayX.Add(rotX);
        rotY = Mathf.Clamp(rotY, minY, maxY);
        rotArrayY.Add(rotY);

        if (rotArrayX.Count >= framesOfSmoothing)
            rotArrayX.RemoveAt(0);

        for (int i = 0; i < rotArrayX.Count; i++)
            avrgXRot += rotArrayX[i];

        if (rotArrayY.Count >= framesOfSmoothing)
            rotArrayY.RemoveAt(0);

        for (int j = 0; j < rotArrayY.Count; j++)
            avrgYRot += rotArrayY[j];

        avrgXRot /= rotArrayX.Count;
        avrgXRot = ClampAngle(avrgXRot, minX, maxX);
        avrgYRot /= rotArrayY.Count;

        Quaternion xQuaternion = Quaternion.AngleAxis(avrgXRot, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis(avrgYRot, Vector3.left);

        transform.localRotation = originalRotation * xQuaternion;
        Camera.main.transform.localRotation = originalCamRotation * yQuaternion;
    }

    public float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}
