﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeting : MonoBehaviour {

    public Transform[] waypoints;
    Seek seek;
    public int index = 0;
    public bool reverse = false;

	void Start () {
        seek = this.gameObject.GetComponent<Seek>();
        seek.target = waypoints[index].position;
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        foreach(Transform t in waypoints)
        {
            Gizmos.DrawSphere(t.position, 2f);
        }
    }

    // Update is called once per frame
    void FixedUpdate () {

        if (Vector3.Distance(transform.position, seek.target) < .1f)
        {
            if (!reverse)
            {
                if (index != waypoints.Length - 1)
                    index++;
                else
                    index = 0;
            }
            else
            {
                if (index != 0)
                    index--;
                else
                    index = waypoints.Length -1;
            }
        }
        seek.target = waypoints[index].position;

	}
}
