﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sight : MonoBehaviour {

    private RaycastHit hit;
    private PanelObject panelObject;
    private PanelControl panelControl;
    private string tempName = "";
    private bool isSelected = false;

    private void Start()
    {
        panelControl = null; 
        panelObject = null;
    }

    void Update () {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log(hit.transform.name);

            if (hit.transform.tag == "PanelButton" && !isSelected)
            {
                panelObject = hit.transform.GetComponent<PanelObject>();
                panelControl = panelObject.GetComponentInParent<PanelControl>();
                panelObject.OnHover();
                tempName = hit.transform.name;
                isSelected = true;
            }
            else if(hit.transform.name != tempName && panelObject != null) { 
                panelObject.OnHoverExit();
                panelObject = null;
                isSelected = false;
            }

            if (isSelected)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    panelControl.DeselectAllObjects();
                    panelObject.OnClicked();
                }
            }
           
            //check to see if panelbutton has been selected.
            //OnHoverExit if raycast hit. name is not the same as panel object

        }

    }
}
