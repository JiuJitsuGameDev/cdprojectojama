﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IInteractable {

    void OnHover();
    void OnHoverExit();
    void OnClicked();
}
