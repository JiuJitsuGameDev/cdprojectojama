﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {

    public float targetY = 4f;
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Open()
    {
        if(transform.position.y < targetY)
        {
            audioSource.Play();
            transform.Translate(Vector3.up * Time.deltaTime);
        }
    }

}
