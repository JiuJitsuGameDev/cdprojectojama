﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOverTest : MonoBehaviour {

    public Material isDefaultMat,
                    isHoverMat,
                    isClickedMat;
    Renderer rend;
    bool isClicked = false;
    private void Start()
    {
        rend = GetComponent<Renderer>();
    }
    private void OnMouseOver()
    {
        rend.material = isHoverMat;
    }

    private void OnMouseExit()
    {
        if(!isClicked)
        rend.material = isDefaultMat;
 
    }
    private void OnMouseDown()
    {
        if (!isClicked)
            rend.material = isClickedMat;
        else
            rend.material = isDefaultMat;
    }
}
