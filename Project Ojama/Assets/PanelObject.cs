﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelObject : MonoBehaviour , IInteractable  {

    DroneAudioSelector droneAudioSelector;
    PanelControl panelControl;
    public enum Note { A, ASHARP, B, C, CSHARP, D, DSHARP, E, F, FSHARP, G, GSHARP };
    public Note note = Note.A; 

    bool isClicked = false;
    public Material isDefaultMat,
                 isHoverMat,
                 isClickedMat;

    Renderer rend;

    public void OnHover()
    {
        if (!isClicked)
            rend.material = isHoverMat;
    }

    public void OnClicked()
    {
        if (!isClicked) { 
            rend.material = isClickedMat;
            isClicked = true;
            panelControl.note = note.ToString();
            droneAudioSelector.UpdateAudio(note.ToString());

        }
        else
            rend.material = isDefaultMat;
    }

    public void Deselect()
    {
        isClicked = false;
        rend.material = isDefaultMat;
    }

    public void OnHoverExit()
    {
        if (!isClicked)
            rend.material = isDefaultMat;
    }
    // Use this for initialization
    void Start () {

        rend = this.GetComponent<Renderer>();
        panelControl = GetComponentInParent<PanelControl>();
        droneAudioSelector = GetComponentInParent<DroneAudioSelector>();
        rend.material = isDefaultMat;
    }

}
