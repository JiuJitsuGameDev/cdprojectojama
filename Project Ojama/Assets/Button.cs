﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour {


    public float pressedHeight = 0.05f,
                 unPressedHeight = 0.3f;
    public bool isPressed;
    public Vector3 target;
    GameObject buttonTransform;
    AudioSource audioSource;

    private void OnTriggerEnter(Collider col)
    {
        if (col.transform.tag == "Player")
        {
            Debug.Log("Is a me PLAYER");
            isPressed = true;
            audioSource.Play();
        }
    }
    private void OnTriggerExit(Collider col)
    {
        if (col.transform.tag == "Player")
        {
            Debug.Log("No player here");
            isPressed = false;
        }
    }

    // Use this for initialization
    void Start () {
        buttonTransform = GameObject.FindGameObjectWithTag("ButtonPlatform");
        target = new Vector3(buttonTransform.transform.position.x, unPressedHeight, buttonTransform.transform.position.z);
        audioSource = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
        target.y = isPressed ? pressedHeight : unPressedHeight;
        if(isPressed && buttonTransform.transform.position.y > target.y)
        {
            buttonTransform.transform.Translate(Vector3.up * -Time.deltaTime);
        }
        if (!isPressed && buttonTransform.transform.position.y < target.y)
        {
            buttonTransform.transform.Translate(Vector3.up * Time.deltaTime);
        }



    }
}
