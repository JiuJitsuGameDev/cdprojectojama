﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class PanelControl : MonoBehaviour {

    List<PanelObject> panelObjects = new List<PanelObject>();
    public bool isSelected = false;
    public string note = "";

    private void Start()
    {
        PanelObject[] panelObject = GetComponentsInChildren<PanelObject>();
        foreach(PanelObject p in panelObject)
        {
            panelObjects.Add(p);
        }
    }

    public void DeselectAllObjects()
    {
        foreach(PanelObject p in panelObjects)
        {
            p.Deselect();
        }
    }
}
